FROM --platform=$TARGETPLATFORM offlineinternet/olip-base:latest

RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) arch='armv7' ;; \
		amd64) arch='amd64' ;; \
		i386) arch='386' ;; \
		arm64) arch='arm64' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac; \
	wget --quiet -O /tmp/traefik.tar.gz "https://github.com/containous/traefik/releases/download/v2.3.1/traefik_v2.3.1_linux_$arch.tar.gz"; \
	tar xzvf /tmp/traefik.tar.gz -C /usr/local/bin traefik; \
	rm -f /tmp/traefik.tar.gz; \
	chmod +x /usr/local/bin/traefik

COPY entrypoint.sh /
COPY traefik.yml /etc/traefik/traefik.yml
EXPOSE 80
ENTRYPOINT ["/entrypoint.sh"]
CMD ["traefik"]
